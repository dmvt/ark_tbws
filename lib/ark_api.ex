defmodule ArkApi do
  use Tesla

  plug Tesla.Middleware.BaseUrl, "https://node.arkoar.group/api"
  plug Tesla.Middleware.JSON

  def account_by_address(address) do
    "/accounts?address=#{address}"
    |> get
    |> Map.get(:body)
    |> Map.get("account")
  end

  def block_get(id) do
    "/blocks/get?id=#{id}"
    |> get
    |> Map.get(:body)
    |> Map.get("block")
  end

  def block_by_height(height) do
    "/blocks?height=#{height}"
    |> get
    |> Map.get(:body)
    |> Map.get("blocks")
    |> List.first
  end

  def blocks_forged(public_key, limit, offset \\ 0, blocks \\ []) do
    if limit > 50 do
      new_blocks =
        "/blocks?generatorPublicKey=#{public_key}&limit=50&offset=#{offset}"
        |> get
        |> Map.get(:body)
        |> Map.get("blocks")

      if Enum.count(new_blocks) == 0 do
        blocks
      else
        blocks_forged(public_key, limit - 50, offset + 50, blocks ++ new_blocks)
      end
    else
      new_blocks =
        "/blocks?generatorPublicKey=#{public_key}&limit=50&offset=#{offset}"
        |> get
        |> Map.get(:body)
        |> Map.get("blocks")

      blocks ++ new_blocks
    end
  end

  def delegate_by_name(name) do
    "/delegates/get?username=#{name}"
    |> get
    |> Map.get(:body)
    |> Map.get("delegate")
  end

  def transactions_by_recipient_id(id) do
    "/transactions?recipientId=#{id}"
    |> get
    |> Map.get(:body)
    |> Map.get("transactions")
  end

  def transactions_by_sender_id(id) do
    "/transactions?senderId=#{id}"
    |> get
    |> Map.get(:body)
    |> Map.get("transactions")
  end

  def voters_by_public_key(public_key) do
    "/delegates/voters?publicKey=#{public_key}"
    |> get
    |> Map.get(:body)
    |> Map.get("accounts")
  end
end
