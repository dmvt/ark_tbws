defmodule Account do
  defstruct [
    :address,
    :balance,
    :multisignatures,
    :public_key,
    :second_public_key,
    :second_signature,
    :u_multisignatures,
    :unconfirmed_balance,
    :unconfirmed_signature
  ]

  def new(%{
    "address" => address,
    "balance" => balance,
    "multisignatures" => multisignatures,
    "publicKey" => public_key,
    "secondPublicKey" => second_public_key,
    "secondSignature" => second_signature,
    "u_multisignatures" => u_multisignatures,
    "unconfirmedBalance" => unconfirmed_balance,
    "unconfirmedSignature" => unconfirmed_signature
  }) do
    %Account{
      address: address,
      balance: balance,
      multisignatures: multisignatures,
      public_key: public_key,
      second_public_key: second_public_key,
      second_signature: second_signature,
      u_multisignatures: u_multisignatures,
      unconfirmed_balance: unconfirmed_balance,
      unconfirmed_signature: unconfirmed_signature
    }
  end

  def new(%{"address" => address}) do
    find(address)
  end

  # Public API

  def find(address) do
    address
    |> ArkApi.account_by_address
    |> new
  end
end
