defmodule Transaction do
  defstruct [
    :amount,
    :asset,
    :blockid,
    :confirmations,
    :fee,
    :id,
    :recipient_id,
    :sender_id,
    :sender_public_key,
    :signature,
    :timestamp,
    :type,
    :vendor_field
  ]

  def new(%{
    "amount" => amount,
    "blockid" => blockid,
    "confirmations" => confirmations,
    "fee" => fee,
    "id" => id,
    "senderId" => sender_id,
    "senderPublicKey" => sender_public_key,
    "signature" => signature,
    "timestamp" => timestamp
  } = payload) do
    %Transaction{
      amount: amount,
      asset: Map.get(payload, "asset"),
      blockid: blockid,
      confirmations: confirmations,
      fee: fee,
      id: id,
      recipient_id: Map.get(payload, "recipientId"),
      sender_id: sender_id,
      sender_public_key: sender_public_key,
      signature: signature,
      timestamp: timestamp,
      vendor_field: Map.get(payload, "vendorField")
    }
  end

  # Public API

  def receiving(id) do
    id
    |> ArkApi.transactions_by_recipient_id
    |> Enum.map(&new/1)
  end

  def sending(id) do
    id
    |> ArkApi.transactions_by_sender_id
    |> Enum.map(&new/1)
  end
end
