defmodule Delegate do
  defstruct [
    :address,
    :approval,
    :missedblocks,
    :producedblocks,
    :productivity,
    :public_key,
    :rate,
    :username,
    :vote
  ]

  def new(%{
    "address" => address,
    "approval" => approval,
    "missedblocks" => missedblocks,
    "producedblocks" => producedblocks,
    "productivity" => productivity,
    "publicKey" => public_key,
    "rate" => rate,
    "username" => username,
    "vote" => vote
  }) do
    %Delegate{
      address: address,
      approval: approval,
      missedblocks: missedblocks,
      producedblocks: producedblocks,
      productivity: productivity,
      public_key: public_key,
      rate: rate,
      username: username,
      vote: vote
    }
  end

  # Public API

  def find_by_name(name) do
    name
    |> ArkApi.delegate_by_name
    |> new
  end

  def voters(%Delegate{public_key: public_key}) do
    public_key
    |> ArkApi.voters_by_public_key
    |> Enum.map(&(Ledger.new(&1["address"])))
  end
end
