defmodule Ledger do
  defstruct [:account, :transactions, :__type_index__]

  @genesis 1490101200

  require Logger

  def new(address) do
    payload =
      %{
        tranactions: [],
        __type_index__: %{}
      }
      |> Map.put(:account, Account.find(address))
      |> add_receiving_transactions
      |> add_sending_transactions
      |> sort_by_time
      |> index_by_type

    struct(__MODULE__, payload)
  end

  # Public API

  def balance(%Ledger{account: %Account{balance: balance}}) do
    balance |> Integer.parse |> elem(0)
  end

  def balance_at(%Ledger{transactions: transactions} = ledger, timestamp) do
    timestamp = timestamp - @genesis
    Enum.reduce(transactions, 0, fn(transaction, balance) ->
      if transaction.timestamp > timestamp do
        balance
      else
        if ledger.account.address == transaction.sender_id do
          balance - transaction.amount - transaction.fee
        else
          balance + transaction.amount
        end
      end
    end)
  end

  def delegate(%Ledger{__type_index__: %{votes: transactions}}) do
    vote =
      transactions
      |> List.first
      |> Map.get(:asset)
      |> Map.get("votes")
      |> List.first

    if String.at(vote, 0) == "+" do
      String.slice(vote, 1..-1)
    end
  end

  def delegate_at(%Ledger{__type_index__: %{votes: transactions}}, timestamp) do
    timestamp = timestamp - @genesis

    key = Enum.reduce(transactions, nil, fn(transaction, public_key) ->
      if transaction.timestamp > timestamp or !is_nil(public_key) do
        public_key
      else
        vote =
          transaction
          |> Map.get(:asset)
          |> Map.get("votes")
          |> List.first

        if String.at(vote, 0) == "+" do
          String.slice(vote, 1..-1)
        else
          :nope
        end
      end
    end)

    if key != :nope, do: key
  end

  def genesis do
    @genesis
  end

  # private

  defp add_receiving_transactions(%{account: account} = payload) do
    Map.put(payload, :transactions, Transaction.receiving(account.address))
  end

  defp add_sending_transactions(
    %{account: account, transactions: transactions} = payload
  ) do
    Map.put(
      payload,
      :transactions,
      account
      |> Map.get(:address)
      |> Transaction.sending
      |> Enum.reduce(transactions, &add_unless_present/2)
    )
  end

  defp add_unless_present(transaction, transactions) do
    if Enum.member?(transactions, transaction) do
      transactions
    else
      [transaction] ++ transactions
    end
  end

  defp index_by_type(%{transactions: transactions} = payload) do
    Map.put(
      payload,
      :__type_index__,
      Enum.reduce(transactions, %{votes: []}, fn(transaction, index) ->
        if is_map(transaction.asset) and
           Map.has_key?(transaction.asset, "votes") do
          Map.put(index, :votes, [transaction] ++ index.votes)
        else
          index
        end
      end)
    )
  end

  defp sort_by_time(%{transactions: transactions} = payload) do
    sorted = Enum.sort_by(transactions, &(&1.timestamp))
    Map.put(payload, :transactions, sorted)
  end
end
