defmodule ArkTbws do
  @moduledoc """
  Documentation for ArkTbws.
  """

  require Logger

  @cut 0.9
  @max_blocks 350

  @doc false
  def calculate(name, block_height, n \\ nil) do
    Logger.info("Loading delegate...")
    delegate = Delegate.find_by_name(name)
    Logger.info("Loading voters...")
    voters = Delegate.voters(delegate)
    Logger.info("Loading blocks...")
    blocks = Block.forged(delegate, @max_blocks)
    Logger.info("Processing blocks...")
    new_blocks = Enum.filter(blocks, &(&1.height > block_height))

    new_blocks =
      if !is_nil(n) and n < Enum.count(new_blocks) do
        Enum.slice(new_blocks, Range.new(0, n))
      else
        new_blocks
      end
    # Pass in block height
    # Pass in number of blocks or none goes to current block
    # block by block for each voter calculating their weight for the reward

    Logger.debug("Paying Blocks: #{Enum.at(new_blocks, 0).height} - #{Enum.at(new_blocks, -1).height}")

    Logger.info("Processing voters...")
    rewards =
      Enum.reduce(voters, %{}, fn(ledger, rewards) ->
        Logger.debug("Processing voter #{ledger.account.address}...")
        reward = Decimal.new(0)

        reward =
          Enum.reduce(new_blocks, reward, fn(block, acc) ->
            timestamp = Ledger.genesis() + block.timestamp
            current_delegate = Ledger.delegate_at(ledger, timestamp)
            if current_delegate == delegate.public_key do
              balance = Ledger.balance_at(ledger, timestamp)

              pool_balance =
                Enum.reduce(voters, 0, fn(voter, total) ->
                  voter_delegate = Ledger.delegate_at(voter, timestamp)
                  if voter_delegate == delegate.public_key do
                    total + Ledger.balance_at(voter, timestamp)
                  else
                    total
                  end
                end)

              weight = Decimal.div(balance, pool_balance)
              cut = Decimal.mult(block.total_forged, weight)

              Decimal.add(acc, cut)
            else
              acc
            end
          end)

        reward =
          reward
          |> Decimal.mult(Decimal.new(@cut))
          |> Decimal.round(0)
          |> Decimal.to_integer

        Map.put(rewards, ledger.account.address, reward)
      end)

      # was the voter voting for us
      # how much did they have
      # how much did the whole pool have
      # percentage weight * reward * @cut

    Enum.each(rewards, fn({address, amount}) ->
      ark = amount/100000000.0
      account = Ledger.new(address)
      balance = Ledger.balance(account)/100000000.0
      Logger.warn("Account #{address} (#{balance}ARK) gets #{ark}ARK rewards.")
    end)

    Logger.info("Total Rewards: #{(rewards |> Map.values |> Enum.sum) / 100000000.0}ARK")
    Logger.info("Paid Blocks: #{Enum.at(new_blocks, 0).height} - #{Enum.at(new_blocks, -1).height}")
  end
end
