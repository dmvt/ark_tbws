defmodule Block do
  defstruct [
    :block_signature,
    :confirmations,
    :generator_id,
    :generator_public_key,
    :height,
    :id,
    :number_of_transactions,
    :payload_hash,
    :payload_length,
    :previous_block,
    :reward,
    :timestamp,
    :total_amount,
    :total_fee,
    :total_forged,
    :version
  ]

  def new(%{
    "blockSignature" => block_signature,
    "confirmations" => confirmations,
    "generatorId" => generator_id,
    "generatorPublicKey" => generator_public_key,
    "height" => height,
    "id" => id,
    "numberOfTransactions" => number_of_transactions,
    "payloadHash" => payload_hash,
    "payloadLength" => payload_length,
    "previousBlock" => previous_block,
    "reward" => reward,
    "timestamp" => timestamp,
    "totalAmount" => total_amount,
    "totalFee" => total_fee,
    "totalForged" => total_forged,
    "version" => version
  }) do
    %Block{
      block_signature: block_signature,
      confirmations: confirmations,
      generator_id: generator_id,
      generator_public_key: generator_public_key,
      height: height,
      id: id,
      number_of_transactions: number_of_transactions,
      payload_hash: payload_hash,
      payload_length: payload_length,
      previous_block: previous_block,
      reward: reward,
      timestamp: timestamp,
      total_amount: total_amount,
      total_fee: total_fee,
      total_forged: total_forged,
      version: version
    }
  end

  # Public API

  def find(id) do
    id
    |> ArkApi.block_get
    |> new
  end

  def find_by_height(height) do
    height
    |> ArkApi.block_by_height
    |> new
  end

  def forged(%Delegate{public_key: public_key}, n \\ 1000) do
    public_key
    |> ArkApi.blocks_forged(n)
    |> Enum.map(&new/1)
  end

  def next(%Block{height: height}) do
    find_by_height(height+1)
  end

  def previous(%Block{previous_block: previous_block}) do
    find(previous_block)
  end
end
